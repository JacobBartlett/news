//
//  FavouritesViewController.swift
//  News
//
//  Created by Bartlett, Jacob (UK - London) on 12/02/2018.
//  Copyright © 2018 Deloitte. All rights reserved.
//

import Foundation
import UIKit

class FavouritesViewController: UICollectionViewController {
    
    // TODO find out if this needs to be rewritten
    
    var favouritedPublications: [FavouritedPublication] = []
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView?.backgroundColor = UIColor.white

        
        NotificationCenter.default.addObserver(self, selector: #selector(self.favouriteRemoved), name: Notification.Name(favouriteRemovedNotificationKey), object: nil)
        
        let topBar = UIView(frame: UIApplication.shared.statusBarFrame)
        topBar.backgroundColor = UIColor.black
        view.addSubview(topBar)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        navigationController?.isNavigationBarHidden = true
        refreshFavourites()
    }
    
    @objc func favouriteRemoved() {
        refreshFavourites()
    }
    
    func refreshFavourites() {
        if let favPubs = FavouriteService.shared.getAllFavourites() {
            favouritedPublications = favPubs
        }
        collectionView?.reloadData()
    }

    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        performSegue(withIdentifier: "showPublicationSegue", sender: favouritedPublications[indexPath.row - 1])
    }

    override func collectionView(_ collectionView: UICollectionView, shouldHighlightItemAt indexPath: IndexPath) -> Bool {
        return indexPath.row > 0
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let publicationVC = segue.destination as? PublicationViewController {
            publicationVC.favouritedPublication = sender as? FavouritedPublication
        }
    }
    
}

extension FavouritesViewController: UICollectionViewDelegateFlowLayout {
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1 + favouritedPublications.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        switch indexPath.row {
        case 0:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "DateHeaderCell", for: indexPath as IndexPath) as! DateHeaderCell
            return cell
            
        default:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FeedCell", for: indexPath as IndexPath) as! FeedCell
            if favouritedPublications.count > 0 {
                cell.favouritedPublication = favouritedPublications[indexPath.row - 1] as FavouritedPublication
                cell.isFavourite = true
                cell.headlineImage.cropCorners()
            }
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if indexPath.row == 0 {
            return CGSize(width: collectionView.bounds.size.width, height: 70.0)
        }
        else {
            if let headlineText = favouritedPublications[indexPath.row - 1].title,
                let fontStyle = UIFont(name: "SFUIDisplay-Bold", size: 28.0) {
                let boundaryBox = headlineText.boundingRect(with: CGSize(width: collectionView.bounds.size.width - 40, height: .greatestFiniteMagnitude),
                                                            options: NSStringDrawingOptions.usesLineFragmentOrigin,
                                                            attributes: [.font: fontStyle,
                                                                         .kern: -1.0], // WARNING - keep this kerning equal to the FeedCell headlineLabel character spacing (-1.0 at time of writing)
                    context: nil)
                let labelHeight = Double(ceil(boundaryBox.height))
                return CGSize(width: Double(collectionView.bounds.size.width), height: 272.0 + labelHeight)
            }
        }
        return CGSize(width: Double(collectionView.bounds.size.width), height: 343.0)
        
    }
}





