//
//  FeedCell.swift
//  News
//
//  Created by Power, Eoin (UK - London) on 30/01/2018.
//  Copyright © 2018 Deloitte. All rights reserved.
//

import UIKit

class FeedCell: UICollectionViewCell {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var newsTypeLabel: UILabel!
    @IBOutlet weak var headlineImage: UIImageView!
    @IBOutlet weak var newsSourceImage: UIImageView!
    @IBOutlet weak var favouriteIcon: UIImageView!
    @IBOutlet weak var headlineLabel: UILabel!
    @IBOutlet weak var dayPostedLabel: UILabel!
    @IBOutlet var gradientView: GradientView!
    
    //constraints
    @IBOutlet var fullCellTitlePositionConstraint: NSLayoutConstraint!
    @IBOutlet var horizontalCellTitlePositionConstraint: NSLayoutConstraint!
    @IBOutlet var verticalCellTitlePositionConstraint: NSLayoutConstraint!
    
    var isSingleNewspaper: Bool = false
    
    var isFavourite: Bool = false {
        didSet {
            if isFavourite {
                favouriteIcon.image = UIImage(named: "favouriteIconFull")
            } else {
                favouriteIcon.image = UIImage(named: "favouriteIconEmpty")
            }
        }
    }
    
    var publication: RSSFeedItem? {
        didSet {
            if let pub = publication {
                let genericPub = GenericPublication(dateAsString: pub.date, dateAsDate: nil, title: pub.title, category: pub.category, newsSource: pub.newsSource, imageUrl: pub.imageUrl)
                setUpCellContent(genericPub)
            }
        }
    }
    
    var favouritedPublication: FavouritedPublication? {
        didSet {
            if let pub = favouritedPublication {
                let genericPub = GenericPublication(dateAsString: nil, dateAsDate: pub.date, title: pub.title, category: pub.category, newsSource: pub.newsSource, imageUrl: pub.imageUrl)
                setUpCellContent(genericPub)
            }
        }
    }
    
    private func setUpCellContent(_ genericPub: GenericPublication) {
        setupLabels(genericPub)
        setupPositioning()
        setupImages(genericPub)
    }
    
    private func setupLabels(_ pub: GenericPublication) {
        if (pub.dateAsString != nil) {
            dayPostedLabel.text = pub.dateAsString?.millisecondStringToDate()?.formatTimeSince()
        } else {
            dayPostedLabel.text = pub.dateAsDate?.formatTimeSince()
        }
        headlineLabel.text = pub.title
        
        if let source = pub.newsSource {
            newsTypeLabel.text = pub.category?.formatCategory(newsSource: source)
        }
        applyLetterSpacing()
    }
    
    private func setupPositioning() {
        if isSingleNewspaper {
            if let fullWidthConstraint = fullCellTitlePositionConstraint {
                fullWidthConstraint.constant = 2
            }
            if let horizontalConstraint = horizontalCellTitlePositionConstraint {
                horizontalConstraint.constant = 2
            }
            if let verticalConstraint = verticalCellTitlePositionConstraint {
                verticalConstraint.constant = 2
            }
        }
        // else block not required for the aggregated feed - constant of 30 set programmatically
    }
    
    private func setupImages(_ pub: GenericPublication) {
        if isSingleNewspaper {
            newsSourceImage.isHidden = true
        }
        else {
            if let source = pub.newsSource {
                setNewsSourceLogo(source)
                newsSourceImage.contentMode = .scaleAspectFit
            }
        }
        headlineImage.setImage(pub.imageUrl, pub.newsSource)
    }
    
    func setNewsSourceLogo(_ source: String) {
        switch source {
        case "BBC News":
            newsSourceImage.image = UIImage(named: "bbcNews")
            
        case "The Guardian":
            newsSourceImage.image = UIImage(named: "theGuardian")
            
        case "The Daily Mail":
            newsSourceImage.image = UIImage(named: "dailyMail")
            
        case "The Independent":
            newsSourceImage.image = UIImage(named: "independent")
            
        case "Politico":
            newsSourceImage.image = UIImage(named: "politico")
    
        default:
            newsSourceImage.image = UIImage(named: "deloitteLogoBlack")
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(favouriteIconPressed))
        favouriteIcon.isUserInteractionEnabled = true
        favouriteIcon.addGestureRecognizer(tapGestureRecognizer)
    }
    
    func applyLetterSpacing() {
        newsTypeLabel.applyCharacterSpacing(of: 2.67)
        headlineLabel.applyCharacterSpacing(of: -1.0) // WARNING - do not change this without changing the FeedViewController "heightForItemAt" function which uses this kerning (-1.0) to calculate line size
        dayPostedLabel.applyCharacterSpacing(of: 0.0)
    }
    
    @objc func favouriteIconPressed() {
        if let publication = publication {
            if isFavourite {                      
                FavouriteService.shared.removeFavouritedPublication(id: publication.id)
                isFavourite = false
            } else {
                FavouriteService.shared.saveFavouritedPublication(publication)
                isFavourite = true
            }
        } else {
            if let favouritedPublication = favouritedPublication {            
                let alertVC = UIAlertController(title: nil, message: "Remove publication from favourites?", preferredStyle: .alert)
                let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
                let okAction = UIAlertAction(title: "Remove", style: .destructive, handler: { action in
                    FavouriteService.shared.removeFavouritedPublication(id: favouritedPublication.id)
                    self.isFavourite = false
                    NotificationCenter.default.post(name: Notification.Name(favouriteRemovedNotificationKey), object: self)
                })
                alertVC.addAction(cancelAction)
                alertVC.addAction(okAction)
                self.window?.rootViewController?.present(alertVC, animated: true, completion: nil)
            }
        }
    }
    
}
