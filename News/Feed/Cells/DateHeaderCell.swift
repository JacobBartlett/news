//
//  DateHeaderCell.swift
//  News
//
//  Created by Power, Eoin (UK - London) on 30/01/2018.
//  Copyright © 2018 Deloitte. All rights reserved.
//

import UIKit
import Foundation

class DateHeaderCell: UICollectionViewCell {
    
    @IBOutlet var containerView: UIView!
    @IBOutlet weak var dayLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var logoImageView: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        configureCell()
    }
    
    private func configureCell() {
        dayLabel.text = Date().getDayOfWeek()?.uppercased()
        dateLabel.text = Date().getDateAndMonth().uppercased()
        logoImageView.image = UIImage(named: "deloitteLogoD")
        applyLetterSpacing()
    }
    
    func applyLetterSpacing() {
        dayLabel.applyCharacterSpacing(of: -0.83)
        dateLabel.applyCharacterSpacing(of: -0.83)
    }
    
}
