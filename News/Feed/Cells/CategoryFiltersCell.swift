//
//  CategoryFiltersCell.swift
//  News
//
//  Created by Bartlett, Jacob (UK - London) on 02/03/2018.
//  Copyright © 2018 Deloitte. All rights reserved.
//

import Foundation
import UIKit

protocol CategoryFilterButtonDelegate {
    func homeSelected(_ category: String)
    func categorySelected(_ category: String)
}

class CategoryFiltersCell: UICollectionViewCell {
    
    var newsSource: String = ""
    var categories: [[String]] = []
    var delegate: CategoryFilterButtonDelegate!
    
    var rendered = false
    
    @IBOutlet var scrollView: UIScrollView!
    @IBOutlet var containerView: UIView!
    @IBOutlet var buttonStack: UIStackView!
    @IBOutlet var homeButton: UIButton!
        
    func createCategoryButtons() {
        let numberOfButtons = categories[1].count + 1
        buttonStack.distribution = .equalSpacing
        
        var tagIndex = 0
        for category in categories[1] {
            let xVal = 150 * (tagIndex + 1)
            buttonStack.addArrangedSubview(createButton(category, xVal, tagIndex))
            tagIndex += 1
        }
        buttonStack.spacing = 30.0
    }
    
    func createButton(_ category: String, _ xVal: Int, _ tagIndex: Int) -> UIButton {
        let categoryButton = UIButton(frame: CGRect(x: (xVal), y: -30, width: 150, height: 30))
        categoryButton.setTitle(category, for: .normal)
        categoryButton.backgroundColor = UIColor.white
        categoryButton.setTitleColor(UIColor(red: 0.0, green: 122.0/255.0, blue: 1.0, alpha: 1.0), for: .normal)
        categoryButton.titleLabel?.font = categoryButton.titleLabel?.font.withSize(15)
        categoryButton.tag = tagIndex
        categoryButton.addTarget(self, action: #selector(categoryButtonPressed), for: .touchUpInside)
        return categoryButton
    }
    
    @IBAction func homeButtonPressed(_ sender: Any) {
        removeBoldButtons()
        homeButton.titleLabel?.font = UIFont.systemFont(ofSize: 15.0, weight: .semibold)
        delegate.homeSelected("")
    }
    
    @objc func categoryButtonPressed(_ sender: UIButton) {
        removeBoldButtons()
        sender.titleLabel?.font = UIFont.systemFont(ofSize: 15.0, weight: .semibold)
        let tagIndex = sender.tag
        print(categories[0][tagIndex])
        delegate.categorySelected(categories[0][tagIndex])
    }
    
    func removeBoldButtons() {
        for view in buttonStack.subviews as [UIView] {
            if let button = view as? UIButton {
                button.titleLabel?.font = UIFont.systemFont(ofSize: 15.0, weight: .regular)
            }
        }
    }
    
}
