//
//  AlertHeaderCell.swift
//  News
//
//  Created by Power, Eoin (UK - London) on 12/02/2018.
//  Copyright © 2018 Deloitte. All rights reserved.
//

import UIKit
import Foundation

class SectionHeaderCell: UICollectionViewCell {
    
    @IBOutlet weak var sectionLabel: UILabel!
    @IBOutlet weak var moreArrowImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        configureCell()
    }
    
    private func configureCell() {
        sectionLabel.text = "PUBLICATION"
        moreArrowImageView.image = UIImage(named: "moreArrow")
        applyLetterSpacing()
    }
    
    func applyLetterSpacing() {
        sectionLabel.applyCharacterSpacing(of: -0.83)
    }
    
}

