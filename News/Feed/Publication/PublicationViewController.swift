//
//  PublicationViewController.swift
//  News
//
//  Created by Bartlett, Jacob (UK - London) on 07/02/2018.
//  Copyright © 2018 Deloitte. All rights reserved.
//

import Foundation
import UIKit
import PDFGenerator

class PublicationViewController: UIViewController, UIDocumentInteractionControllerDelegate {
    
    @IBOutlet var scrollView: UIScrollView!
    
    // TODO - enable paging to go to next article in list 
    
    @IBOutlet var contentView: UIView!
    @IBOutlet var contentHeight: NSLayoutConstraint!
    
    var publication: RSSFeedItem?
    var favouritedPublication: FavouritedPublication?
    
    var fromSingleNewspaperFeed: Bool = false
    
    @IBOutlet weak var headlineLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var bodyText: UILabel!
    @IBOutlet var viewInBrowserButton: UIButton!
    @IBOutlet var downloadAsPDFButton: UIButton!
    
    var isFavourite: Bool = false {
        didSet {
            if isFavourite {
                favouriteIcon = UIImage(named: "blueHeartFull")
                resetRightBarButtonIcons()
            } else {
                favouriteIcon = UIImage(named: "blueHeartEmpty")
                resetRightBarButtonIcons()
            }
        }
    }
    
    var favouriteIcon = UIImage(named: "blueHeartEmpty")
    
    override func viewDidLoad() {
        var titleForCalculatingHeight: String?
        var bodyTextForCalculatingHeight: String?
        if let publication = publication {
            headlineLabel.text = publication.title
            dateLabel.text = publication.date?.millisecondStringToDate()?.getDayAndMonthAndYear()
            bodyText.text = publication.bodyText
            titleForCalculatingHeight = publication.title
            bodyTextForCalculatingHeight = publication.bodyText
            imageView.setImage(publication.imageUrl, publication.newsSource)
            imageView.cropCorners()
        }
        if let favPub = favouritedPublication {
            headlineLabel.text = favPub.title
            dateLabel.text = favPub.date?.getDayAndMonthAndYear()
            bodyText.text = favPub.bodyText
            titleForCalculatingHeight = favPub.title
            bodyTextForCalculatingHeight = favPub.bodyText
            imageView.setImage(favPub.imageUrl, favPub.newsSource)
            imageView.cropCorners()
            isFavourite = true
        }
        applyLetterSpacing()
        if let title = titleForCalculatingHeight,
            let bodyText = bodyTextForCalculatingHeight {
            contentHeight.constant = windowHeight(title, bodyText)
        }
    }
    
    func applyLetterSpacing() {
        headlineLabel.applyCharacterSpacing(of: -1.0)
        dateLabel.applyCharacterSpacing(of: 0.0)
    }

    // TODO - remove force unwraps
    func windowHeight(_ title: String, _ bodyText: String) -> CGFloat {
        return CGFloat(403.0
                    + UILabel().determineHeaderHeight(text: title, fontSize: 28.0, width: (Double(self.view.frame.width - 40.0)))
                    + UILabel().determineTextHeight(text: bodyText, fontSize: 17.0, width: Double((self.view.frame.width - 40.0))))
        // 343.0 = constraints (top to bottom) => 11 + 11 + 11 + 11 + 30 + 30 + 30;
        //         + item heights              => 21 (date), 188 (image), 2 x 30 (button);
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        configureNavigationBar()
    }
    
    @IBAction func viewInBrowserButtonPressed(_ sender: Any) {
        openArticleInBrowser()
    }
    
    @IBAction func downloadAsPDFButtonPressed(_ sender: Any) {
        getPDF()
    }
    
    @objc func shareButtonPressed() {
        var sharedLink = ""
        if let link = (publication?.link) {
            sharedLink = link
        }
        let activityVC = UIActivityViewController(activityItems: ["\(sharedLink) \n \nSent from Deloitte News"],
                                                    applicationActivities: nil)
        self.present(activityVC, animated: true, completion: nil)
    }
    
    @objc func favouriteButtonPressed() {
        if let publication = publication {
            if isFavourite {
                FavouriteService.shared.removeFavouritedPublication(id: publication.id)
                isFavourite = false
            } else {
                FavouriteService.shared.saveFavouritedPublication(publication)
                isFavourite = true
            }
        }
        if let favouritedPublication = favouritedPublication {
            let alertVC = UIAlertController(title: nil, message: "Remove publication from favourites?", preferredStyle: .alert)
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
            let okAction = UIAlertAction(title: "Remove", style: .destructive, handler: { action in
                FavouriteService.shared.removeFavouritedPublication(id: favouritedPublication.id)
                self.isFavourite = false
                NotificationCenter.default.post(name: Notification.Name(favouriteRemovedNotificationKey), object: self)
                self.navigationController?.popViewController(animated: true)
            })
            alertVC.addAction(cancelAction)
            alertVC.addAction(okAction)
            self.present(alertVC, animated: true, completion: nil)
        }
    }
    
    // MARK: - Navigation Bar
    
    func configureNavigationBar() {
        navigationController?.isNavigationBarHidden = false
        applyLogoToNavBar()
        addButtonToLogo()
        // TODO - Left bar button item should not contain the word 'back', just the arrow
        if let publication = publication {
            isFavourite = configureFavouriteStatus(publication)
        }
        resetRightBarButtonIcons()
    }
    
    func applyLogoToNavBar() {
        if let publicationSource = publication?.newsSource {
            navigationItem.applyLogoToTitleView(publicationSource)
        }
        if let favouriteSource = favouritedPublication?.newsSource {
            navigationItem.applyLogoToTitleView(favouriteSource)
        }
    }
    
    func addButtonToLogo() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.goToNewsSource))
        navigationItem.titleView?.isUserInteractionEnabled = true
        navigationItem.titleView?.addGestureRecognizer(tap)
    }
    
    func resetRightBarButtonIcons() {
        let shareButton = UIBarButtonItem(barButtonSystemItem: .action, target: self, action: #selector(shareButtonPressed))
        let favouriteButton = UIBarButtonItem(image: favouriteIcon, style: .plain, target: self, action: #selector(favouriteButtonPressed))
        navigationItem.rightBarButtonItems = [shareButton, favouriteButton]
    }
    
    @objc func goToNewsSource() {
        if fromSingleNewspaperFeed {
            navigationController?.popViewController(animated: true)
        } else {
            goToSingleNewspaper()
        }
    }
    
    func goToSingleNewspaper() {
        let newspaperVC = UIStoryboard(name: "Feed", bundle: nil).instantiateViewController(withIdentifier: "FeedViewController") as! FeedViewController
        newspaperVC.isSingleNewspaper = true
        if let newspaperName = getNewspaperName() {
            newspaperVC.newspaperName = newspaperName
            self.navigationController?.pushViewController(newspaperVC, animated: true)
        }
    }
    
    func getNewspaperName() -> String? {
        if let publicationSource = publication?.newsSource {
            return publicationSource
        }
        if let favouriteSource = favouritedPublication?.newsSource {
            return favouriteSource
        }
        return nil
    }
    
    // MARK: - View in Browser
    
    func openArticleInBrowser() {
        if let urlString = getUrlString(),
            let url = URL(string: urlString) {
            UIApplication.shared.open(url)
        }
    }
    
    func getUrlString() -> String? {
        if let publicationUrl = publication?.url {
            return publicationUrl
        }
        if let favouriteUrl = favouritedPublication?.url {
            return favouriteUrl
        }
        return nil
    }

    // MARK: - Download as PDF
    
    func getPDF() {
        let pdfUrl = generatePDFWithUrl()
        let destinationFileUrl = getDestinationFileUrl()
        savePDFLocally(pdfUrl, destinationFileUrl)

        if let url = destinationFileUrl {
            presentPDF(url)
        }
    }

    func generatePDFWithUrl() -> URL? {
        let view = contentView
        let title = getFirstWordOfTitle() // TODO why is this duplicated
        let tempDirectory = URL(fileURLWithPath: NSTemporaryDirectory().appending("\(title).pdf"))
        do {
            try PDFGenerator.generate(view!, to: tempDirectory)
            return tempDirectory
        } catch (let error) {
            print(error)
            return nil
        }
    }
    
    func getDestinationFileUrl() -> URL? {
        let documentsUrl = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first as URL?
        let docTitle = getFirstWordOfTitle() // TODO why is this duplicated
        return documentsUrl?.appendingPathComponent("\(docTitle).pdf")
    }
        
    func savePDFLocally(_ pdfUrl: URL?, _ destinationFileUrl: URL?) {
        do {
            try FileManager.default.copyItem(at: pdfUrl!, to: destinationFileUrl!)
        } catch (let writeError) {
            print("Error creating file: \(writeError)")
        }
    }
        
    func presentPDF(_ url: URL?) {
        let documentInteractionController = UIDocumentInteractionController(url: url!)
        documentInteractionController.delegate = self
        documentInteractionController.presentPreview(animated: true)
        contentView.frame = CGRect(x: 0, y: 0, width: contentView.frame.size.width, height: contentView.frame.size.height)
    }
    
    func documentInteractionControllerViewControllerForPreview(_ controller: UIDocumentInteractionController) -> UIViewController {
        return self
    }
    
    func getFirstWordOfTitle() -> String {
        if let title = publication?.title {
            return String(title.split(separator: " ")[0])
        }
        if let title = favouritedPublication?.title {
            return String(title.split(separator: " ")[0])
        }
        return "no-title-available" // should never occur
    }
    
    // MARK: - Favourite
    
    func configureFavouriteStatus(_ feedItem: RSSFeedItem) -> Bool {
        var favouritedPublicationIds: [String] = []
        if let favPubs = FavouriteService.shared.getAllFavourites() {
            for pub in favPubs {
                favouritedPublicationIds.append(pub.id)
            }
        }
        
        for id in favouritedPublicationIds {
            if id == feedItem.id {
                return true
            }
        }
        return false
    }
    
    
}
