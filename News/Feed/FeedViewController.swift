//
//  FeedViewController.swift
//  News
//
//  Created by Power, Eoin (UK - London) on 30/01/2018.
//  Copyright © 2018 Deloitte. All rights reserved.
//

import UIKit

class FeedViewController: UICollectionViewController, CategoryFilterButtonDelegate {
    
    var feed: [RSSFeedItem] = []
    var favouritedPublicationIds: [String] = []
    
    var isSingleNewspaper: Bool = false
    var newspaperName: String?
    var isFilteredByCategory: Bool = false

    var refresher: UIRefreshControl!
    
    var horizontalItemsLeftCellHeight: Double = 0.0
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        if isSingleNewspaper {
            return .default
        } else {
            return .lightContent
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setupNotifications()
        setupRefreshControl()
        collectionView?.alwaysBounceVertical = true

        if isSingleNewspaper {
            fetchFeedData(filterCategory: nil)
        }
    }
    
    func setupNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(syncNotificationReceived), name: Notification.Name(syncNotificationKey), object: nil)
    }
    
    func setupRefreshControl() {
        refresher = UIRefreshControl()
        refresher.addTarget(self, action: #selector(refreshFeed), for: .valueChanged)
        collectionView?.refreshControl = refresher
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if isSingleNewspaper {
            if let name = newspaperName {
                navigationController?.isNavigationBarHidden = false
                navigationItem.applyLogoToTitleView(name)
            }
        } else {
            let topBar = UIView(frame: UIApplication.shared.statusBarFrame)
            topBar.backgroundColor = UIColor.black
            view.addSubview(topBar)
            navigationController?.isNavigationBarHidden = true
        }
        favouritedPublicationIds = []
        if let favPubs = FavouriteService.shared.getAllFavourites() {
            for pub in favPubs {
                favouritedPublicationIds.append(pub.id)
            }
        }
    }
    
    @objc func syncNotificationReceived() {
        fetchFeedData(filterCategory: nil)
    }
    
    func fetchFeedData(filterCategory: String?) {
        do {
            feed = try ContentfulService.shared.store.fetchAll(type: RSSFeedItem.self, predicate: NSPredicate(value: true))
            configureFeed(filterCategory)
            asyncReloadCollectionView()
        } catch {
            print("Error fetching data from Contentful offline store")
        }
    }
    
    private func configureFeed(_ filterCategory: String?) {
        filterSingleNewspaperFeed()
        filterFeedByCategory(filterCategory)
        sortFeedByDate()
    }

    private func filterSingleNewspaperFeed() {
        if isSingleNewspaper {
            feed = feed.filter( { $0.newsSource == newspaperName } )
        }
    }

    private func filterFeedByCategory(_ filterCategory: String?) {
        if let cat = filterCategory {
            feed = feed.filter( { $0.category == cat } )
            isFilteredByCategory = true
        }
    }

    private func sortFeedByDate() {
        feed.sort {
            guard let date0 = $0.date, let date1 = $1.date else { return false }
            return date0 > date1
        }
    }
    
    private func asyncReloadCollectionView() {
        DispatchQueue.main.async {
            self.collectionView?.reloadData()
        }
    }

    @objc func refreshFeed() {
        contentfulSync()
        stopRefresher()
    }
    
    private func contentfulSync() {
        ContentfulService.sync { (success, error) in
            if error == nil {
                print("Successfully synced with Contentful!")
                NotificationCenter.default.post(name: Notification.Name(syncNotificationKey), object: self)
            } else {
                print("Contentful sync at launch error: \(error!)")
            }
        }
    }
    
    func stopRefresher() {
        self.refresher.endRefreshing()
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        performSegue(withIdentifier: "showPublicationSegue", sender: feed[indexPath.row - 1])
    }

    override func collectionView(_ collectionView: UICollectionView, shouldHighlightItemAt indexPath: IndexPath) -> Bool {
        return indexPath.row > 0
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let publicationVC = segue.destination as? PublicationViewController {
            if let feedItem = sender as? RSSFeedItem {
                
                // TODO - find out if this kills favourites or something similar - maybe add an else?
                
                publicationVC.publication = feedItem
                publicationVC.fromSingleNewspaperFeed = isSingleNewspaper
                publicationVC.isFavourite = configureFavouriteStatus(feedItem)
            }
        }
    }

    private func retreiveFormattedCategories(_ newspaperName: String) -> [[String]]  {
        let rawCategories = parseCategoriesFromFeed()
        var formattedCategories: [String] = []
        for category in rawCategories {
            formattedCategories.append(category.formatCategory(newsSource: newspaperName))
        }
        return [rawCategories, formattedCategories]
    }
    
    private func parseCategoriesFromFeed() -> [String] {
        var newsCategories: [String] = []
        for item in feed {
            if let cat = item.category {
                if categoryNotYetInList(cat: cat, list: newsCategories) {
                    newsCategories.append(cat)
                }
            }
        }
        return newsCategories
    }
    
    private func categoryNotYetInList(cat: String, list: [String]) -> Bool {
        for listItem in list {
            if cat == listItem {
                return false
            }
        }
        return true
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1 + feed.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if indexPath.row == 0 {
            if isSingleNewspaper {
                return createCategoryFiltersCell()

            } else {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "DateHeaderCell", for: indexPath as IndexPath) as! DateHeaderCell
                return cell
            }
        }
        
        let feedItem = feed[indexPath.row - 1] as RSSFeedItem
        switch ((indexPath.row - 1) % 5) {
        case 0:
            return createFeedCell(indexPath, feedItem, alignment: "FeedCellFull")
            
        case 1, 2:
            if lastCellInFeed(indexPath) {
                return createFeedCell(indexPath, feedItem, alignment: "FeedCellVertical")
            }
            return createFeedCell(indexPath, feedItem, alignment: "FeedCellHorizontal")
            
        case 3, 4:
            return createFeedCell(indexPath, feedItem, alignment: "FeedCellVertical")
            
        default:
            return UICollectionViewCell() // the switch statement is exhaustive so this would never be reached
            
        }
    }

    func createCategoryFiltersCell() -> UICollectionViewCell {
        let cell = collectionView?.dequeueReusableCell(withReuseIdentifier: "categoryFiltersCell", for: IndexPath(item: 0, section: 0)) as! CategoryFiltersCell
        if !isFilteredByCategory {
            cell.delegate = self
            if let newspaperName = newspaperName {
                if !cell.rendered {
                    cell.newsSource = newspaperName
                    cell.categories = retreiveFormattedCategories(newspaperName)
                    cell.homeButton.setTitle("Home", for: .normal)
                    cell.homeButton.titleLabel?.font = UIFont.systemFont(ofSize: 15.0, weight: .semibold)
                    cell.createCategoryButtons()
                    cell.rendered = true
                }
            }
        }
        return cell
    }
        
    
    private func createFeedCell(_ indexPath: IndexPath, _ feedItem: RSSFeedItem, alignment identifier: String) -> FeedCell {
        let cell = collectionView?.dequeueReusableCell(withReuseIdentifier: identifier, for: indexPath as IndexPath) as! FeedCell
        return configureFeedCell(item: feedItem, cell: cell)
    }
    
    private func configureFeedCell(item: RSSFeedItem, cell: FeedCell) -> FeedCell {
        cell.isSingleNewspaper = isSingleNewspaper // cell.isSingleNewspaper needs to be set before cell.publication
        cell.publication = item  // cell.isSingleNewspaper needs to be set before cell.publication
        cell.headlineImage.cropCorners()
        cell.isFavourite = configureFavouriteStatus(item)
        return cell
    }
    
    private func configureFavouriteStatus(_ feedItem: RSSFeedItem) -> Bool {
        for id in favouritedPublicationIds {
            if id == feedItem.id {
                return true
            }
        }
        return false
    }
    
    func homeSelected(_ category: String) {
        fetchFeedData(filterCategory: nil)
    }
    
    func categorySelected(_ category: String) {
        fetchFeedData(filterCategory: category)
    }
    
}

extension FeedViewController: UICollectionViewDelegateFlowLayout {

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let fullWidth = Double(collectionView.bounds.size.width)
        let halfWidth = Double(collectionView.bounds.size.width - 10)/2.0
        
        if indexPath.row == 0 {
            return setupTopCellSize(fullWidth)
        }
        else {
            return setupFeedCellSize(fullWidth, halfWidth, indexPath)
        }
    }
    
    func setupTopCellSize(_ width: Double) -> CGSize {
        if isSingleNewspaper {
            return CGSize(width: width, height: 30.0)
        }
        else {
            return CGSize(width: width, height: 70.0)
        }
    }
    
    func setupFeedCellSize(_ fullWidth: Double, _ halfWidth: Double, _ indexPath: IndexPath) -> CGSize {
        if let title = feed[indexPath.row - 1].title {
            switch ((indexPath.row - 1) % 5) {
            case 0:
                return calculateFullWidthCellSize(title: title, cellWidth: fullWidth)
                
            case 1:
                if lastCellInFeed(indexPath) {
                    return calculateVerticalCellSize(title: title, cellWidth: fullWidth)
                }
                if let title2 = feed[indexPath.row].title {
                    return calculateLeftHorizontalCellSize(leftTitle: title, rightTitle: title2, cellWidth: halfWidth)
                }
                
            case 2:
                return calculateRightHorizontalCellSize(title: title, cellWidth: halfWidth)
                
            case 3, 4:
                return calculateVerticalCellSize(title: title, cellWidth: fullWidth)
                
            default:
                return defaultCellSize(cellWidth: fullWidth) // this will never happen as the switch is exhaustive
            }
        }
        return defaultCellSize(cellWidth: fullWidth)
    }
    
    private func calculateFullWidthCellSize(title: String, cellWidth: Double) -> CGSize {
        let labelHeight = UILabel().determineHeaderHeight(text: title, fontSize: 28.0, width: (cellWidth - 40))
        let heightWithoutLabel = 242.0 + (isSingleNewspaper ? 0.0 : 26.0)
        return CGSize(width: cellWidth, height: heightWithoutLabel + labelHeight)
    }
    
    private func calculateLoneHorizontalCellSize(title: String, cellWidth: Double) -> CGSize {
        let labelHeight = UILabel().determineHeaderHeight(text: title, fontSize: 20.0, width: cellWidth - 40.0)
        let heightWithoutLabel = 139.0 + (isSingleNewspaper ? 0.0 : 26.0)
        return CGSize(width: cellWidth, height: heightWithoutLabel + labelHeight)
    }
    
    private func calculateLeftHorizontalCellSize(leftTitle: String, rightTitle: String, cellWidth: Double) -> CGSize {
        let labelHeightLeft = UILabel().determineHeaderHeight(text: leftTitle, fontSize: 20.0, width: cellWidth - 40.0)
        let labelHeightRight = UILabel().determineHeaderHeight(text: rightTitle, fontSize: 20.0, width: cellWidth - 40.0)
        let largestHeight = labelHeightLeft > labelHeightRight ? labelHeightLeft : labelHeightRight
        horizontalItemsLeftCellHeight = largestHeight
        let heightWithoutLabel = 139.0 + (isSingleNewspaper ? 0.0 : 26.0)
        return CGSize(width: cellWidth, height: heightWithoutLabel + largestHeight)
    }

    private func calculateRightHorizontalCellSize(title: String, cellWidth: Double) -> CGSize {
        let largestHeight = horizontalItemsLeftCellHeight
        horizontalItemsLeftCellHeight = 0.0
        let heightWithoutLabel = 139.0 + (isSingleNewspaper ? 0.0 : 26.0)
        return CGSize(width: cellWidth, height: heightWithoutLabel + largestHeight)
    }
    
    private func calculateVerticalCellSize(title: String, cellWidth: Double) -> CGSize {
        let labelHeight = UILabel().determineHeaderHeight(text: title, fontSize: 20.0, width: Double(cellWidth) - 162.0)
        let heightWithoutLabel = 64.0 + (isSingleNewspaper ? 0.0 : 26.0)
        let total = labelHeight + heightWithoutLabel
        return CGSize(width: cellWidth, height: (total > 120 ? total : 120))
    }
    
    private func defaultCellSize(cellWidth: Double) -> CGSize {
        return CGSize(width: cellWidth, height: 360.0)
    }
    
    func lastCellInFeed(_ indexPath: IndexPath) -> Bool {
        if (indexPath.row - 1) % 5 == 1 && indexPath.row == feed.count {
            return true
        }
        else {
            return false
        }
    }

}






