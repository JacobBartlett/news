//
//  SearchTableViewCell.swift
//  News
//
//  Created by Bartlett, Jacob (UK - London) on 14/02/2018.
//  Copyright © 2018 Deloitte. All rights reserved.
//

import Foundation
import UIKit

class SearchTableViewCell: UITableViewCell {
    
    @IBOutlet weak var searchTermLabel: UILabel!
    
}
