//
//  String+Extensions.swift
//  News
//
//  Created by Bartlett, Jacob (UK - London) on 08/02/2018.
//  Copyright © 2018 Deloitte. All rights reserved.
//

import Foundation

extension String {
    
    func millisecondStringToDate() -> Date? {
        var seconds = 0.0
        if let milliseconds = Double(self) {
            seconds = milliseconds/1000.0
        }
        let dateFromUnixEpoch = NSDate(timeIntervalSince1970: TimeInterval(seconds))
        return dateFromUnixEpoch as Date
    }
    
    //Optional("2017-12-14T00:00+00:00")
    func contentfulLongStringToDate() -> Date? {
        let dateTime = self.split(separator: "T")
        let date = dateTime[0].split(separator: "-")
        let time = dateTime[1].split(separator: "+")
        let hourMin = time[0].split(separator: ":")

        var dateComponents = DateComponents()
        dateComponents.year = Int(date[0])
        dateComponents.month = Int(date[1])
        dateComponents.day = Int(date[2])
        dateComponents.timeZone = TimeZone(abbreviation: "GMT")
        dateComponents.hour = Int(hourMin[0])
        dateComponents.minute = Int(hourMin[1])
        
        let userCalendar = Calendar.current
        if let returnDate = userCalendar.date(from: dateComponents) {
            return returnDate
        }
        return nil
    }
    
    func formatCategory(newsSource: String) -> String {
        switch newsSource {
        case "BBC News", "The Independent":
            //The Independent and BBC News have the same category structure
            let newsTypeWithoutBBCNews = self.split(separator: "-")[1]
            var newsTypeWithoutBBCNewsString = String(newsTypeWithoutBBCNews)
            newsTypeWithoutBBCNewsString.removeFirst()
            return newsTypeWithoutBBCNewsString
            
        case "The Guardian", "The Daily Mail", "The Independent":
            //The Guardian and The Daily Mail have the same category structure
            let newsTypeWithoutPaperName = self.split(separator: "|")[0]
            return String(newsTypeWithoutPaperName)
            
        case "Politico":
            return self
        
        default:
            return self
        }
    }
    
}
