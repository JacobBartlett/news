//
//  UILabel+Extensions.swift
//  News
//
//  Created by Bartlett, Jacob (UK - London) on 09/02/2018.
//  Copyright © 2018 Deloitte. All rights reserved.
//

import Foundation
import UIKit

extension UILabel {
        
    func applyCharacterSpacing(of spacing: Double) {
        if let labelText = text, labelText.count > 0 {
            let attributedString = NSMutableAttributedString(string: labelText)
            attributedString.addAttribute(NSAttributedStringKey.kern, value: spacing, range: NSRange(location: 0, length: attributedString.length - 1))
            attributedText = attributedString
        }
    }
    
    func determineHeaderHeight(text: String, fontSize: Double, width: Double) -> Double {
        let fontStyle = UIFont(name: "SFUIDisplay-Bold", size: CGFloat(fontSize))
        let boundaryBox = text.boundingRect(with: CGSize(width: width, height: .greatestFiniteMagnitude),
                                            options: NSStringDrawingOptions.usesLineFragmentOrigin,
                                            attributes: [.font: fontStyle as Any,
                                                         .kern: -1.0], // WARNING - keep this kerning equal to the FeedCell headlineLabel character spacing (-1.0 at time of writing)
            context: nil)
        let labelHeight = Double(ceil(boundaryBox.height))
        return labelHeight
    }
    
    func determineTextHeight(text: String, fontSize: Double, width: Double) -> Double {
        let fontStyle = UIFont(name: "SFUIText-Regular", size: CGFloat(fontSize))
        let boundaryBox = text.boundingRect(with: CGSize(width: width, height: .greatestFiniteMagnitude),
                                            options: NSStringDrawingOptions.usesLineFragmentOrigin,
                                            attributes: [.font: fontStyle as Any,
                                                         .kern: 0],
            context: nil)
        let labelHeight = Double(ceil(boundaryBox.height))
        return labelHeight
    }
    
}
