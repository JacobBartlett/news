//
//  DayOfWeek.swift
//  News
//
//  Created by Power, Eoin (UK - London) on 30/01/2018.
//  Copyright © 2018 Deloitte. All rights reserved.
//

import Foundation

extension Date {
    
    func getDayOfWeek() -> String? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEEE"
        return dateFormatter.string(from: self).capitalized
    }
    
    func getDateAndMonth() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMMM d"
        return dateFormatter.string(from: self)
    }
    
    func getDayAndMonthAndYear() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "d MMMM YYYY"
        return dateFormatter.string(from: self)
    }
    
    func formatTimeSince() -> String {
        let calendar = Calendar.current
        let components = calendar.dateComponents([.minute, .hour, .day, .month], from: self, to: Date())
        if let minutes = components.minute,
            let hours = components.hour,
            let days = components.day,
            let months = components.month {
            
            switch months {
            case 0:
                if days < 1 {
                    if hours < 1 {
                        if minutes < 5 { return "just now" }
                        else { return "\(minutes)m ago" }
                    } else { return "\(hours)h ago" }
                }
                if days == 1 { return "1 day ago" }
                if days < 7 { return "\(days) days ago" }
                if days < 14 { return "1 week ago" }
                else { return "\(Int(floor(Double(days)/7.0))) weeks ago" }
            case 1:
                return "1 month ago"
            default:
                return "\(months) months ago"
            }
        }
        return "date not available"
    }

}
