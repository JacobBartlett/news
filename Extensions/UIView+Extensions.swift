//
//  UIView+Extensions.swift
//  News
//
//  Created by Bartlett, Jacob (UK - London) on 14/02/2018.
//  Copyright © 2018 Deloitte. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
    
    func viewToCircle() {
        self.layer.cornerRadius = self.frame.size.width / 2
        self.clipsToBounds = true
    }
    
}
