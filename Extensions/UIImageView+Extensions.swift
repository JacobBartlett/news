//
//  UIImageView+Extensions.swift
//  News
//
//  Created by Bartlett, Jacob (UK - London) on 31/01/2018.
//  Copyright © 2018 Deloitte. All rights reserved.
//

import UIKit
import SDWebImage

extension UIImageView {
    
    func setImage(_ urlString: String?, _ source: String?) {
        if let urlString = urlString,
            let source = source {
            switch source {
            case "BBC News":
                sd_setImage(with: URL(string: urlString), placeholderImage: UIImage(named: "bbcNewsSquare"))
            
            case "The Guardian":
                sd_setImage(with: URL(string: urlString), placeholderImage: UIImage(named: "theGuardianSquare"))
                
            case "The Daily Mail":
                sd_setImage(with: URL(string: urlString), placeholderImage: UIImage(named: "dailyMailSquare"))
                
            case "The Independent":
                sd_setImage(with: URL(string: urlString), placeholderImage: UIImage(named: "independentSquare"))
                
            case "Politico":
                sd_setImage(with: URL(string: urlString), placeholderImage: UIImage(named: "politicoSquare"))
                
            default:
                image = UIImage(named: "errorImage")
            }
            
        } else {
            image = UIImage(named: "errorImage")
        }
    }
    
    func cropCorners() {
        self.layer.cornerRadius = 3
    }
    
}
