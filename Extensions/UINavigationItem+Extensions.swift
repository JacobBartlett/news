//
//  UINavigationItem+Extensions.swift
//  News
//
//  Created by Bartlett, Jacob (UK - London) on 23/02/2018.
//  Copyright © 2018 Deloitte. All rights reserved.
//

import Foundation
import UIKit

extension UINavigationItem {
    
    func applyLogoToTitleView(_ newsSource: String) {
        var logoImageView = UIImageView()
        
        if newsSource == "BBC News" {
            logoImageView = setNewsSourceLogo(logoImageView: logoImageView, logoImageName: "bbcNews")
        }
        if newsSource == "The Guardian" {
            logoImageView = setNewsSourceLogo(logoImageView: logoImageView, logoImageName: "theGuardian")
        }
        if newsSource == "The Daily Mail" {
            logoImageView = setNewsSourceLogo(logoImageView: logoImageView, logoImageName: "dailyMail")
        }
        if newsSource == "The Independent" {
            logoImageView = setNewsSourceLogo(logoImageView: logoImageView, logoImageName: "independent")
        }
        if newsSource == "Politico" {
            logoImageView = setNewsSourceLogo(logoImageView: logoImageView, logoImageName: "politico")
        }
        
        // TODO - Left bar button item should not contain the word 'back', just the arrow
        self.titleView = logoImageView
    }
    
    private func setNewsSourceLogo(logoImageView: UIImageView, logoImageName: String) -> UIImageView {
        logoImageView.frame = CGRect(x: 0, y: 0, width: 120.0, height: 30.0)
        logoImageView.contentMode = .scaleAspectFit
        let newsSourceLogo = UIImage(named: logoImageName)
        let scaledNewsSourceLogo = newsSourceLogo?.resize(to: CGSize(width: 120.0, height: 30.0))
        logoImageView.image = scaledNewsSourceLogo
        return logoImageView
    }
    
}
