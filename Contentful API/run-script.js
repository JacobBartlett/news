var contentfulDelete = require('./contentful-delete.js');
var bbc = require('./rss feeds/bbc.js');
var guardian = require('./rss feeds/guardian.js');
// var dailyMail = require('./rss feeds/daily-mail.js');
var independent = require('./rss feeds/independent.js');
var politico = require('./rss feeds/politico.js');

// deleteAllEntries();
// setTimeout(loadAllFeeds, 20000);
// setTimeout(removeDuplicates, 60000);

loadAllFeeds();

function loadAllFeeds() {
    bbc.data.loadAllFeeds();
    guardian.data.loadAllFeeds();
    independent.data.loadAllFeeds();
    politico.data.loadAllFeeds();
}

function deleteAllEntries() {
    contentfulDelete.data.deleteAllEntriesInSpace();
}

function removeDuplicates() {
    contentfulDelete.data.removeDrafts();
}