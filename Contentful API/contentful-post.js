var contentfulManagement = require('contentful-management');

var client = contentfulManagement.createClient({
  // This is the access token for this space. Normally you get both ID and the token in the Contentful web app
  accessToken: 'CFPAT-89983f855135f9f2537ca092f48b75519cb17e0abbd96c093a32e58fd928d8a0'
})

var exportedMethods = {
  // the BBC function works for the Independent & Daily Mail rss feeds also
  publishBBCEntriesToSpace: function(source, rssCategory, rssResponseItems) {
    for (i=0; i<rssResponseItems.length; i++) {
      
      // clean strings include a regex that fixes the apostrophes from &apos; to '
      let title = String(rssResponseItems[i].title);
      let cleanTitle = title.replace(/&apos;/g, "'");

      let description = String(rssResponseItems[i].description);
      let cleanDescription = description.replace(/&apos;/g, "'");

      let link = String(rssResponseItems[i].link);
      let url = String(rssResponseItems[i].url);

      // var date = "0";
      // if (typeof String(rssResponseItems[i].created) != 'undefined') {
      let date = String(rssResponseItems[i].created);
      // }

      // if (typeof String(rssResponseItems[i].media.thumbnail[0].url[0]) != "undefined") {
      let imageUrl = String(rssResponseItems[i].media.thumbnail[0].url[0]);
      // }
  
      client.getSpace('695flcgy8uvq')
      .then((space) => space.createEntry('rssFeed', {
        fields: {
          title: {
            'en-GB': cleanTitle
          },
          newsSource: {
            'en-GB': source
          },
          category: {
            'en-GB': rssCategory
          },
          description: {
            'en-GB': cleanDescription
          },
          link: {
            'en-GB': link
          },
          url: {
            'en-GB': url
          },
          date: {
            'en-GB': date
          },
          imageUrl: {
            'en-GB': imageUrl
          }
        }
      }))
      .then((entry) => entry.publish())
      .then((entry) => console.log(source + ": " + cleanTitle + "\' published successfully!"))
      .catch(console.error)
    }  
  },

  publishGuardianEntriesToSpace: function(source, rssCategory, rssResponseItems) {
    for (i=0; i<rssResponseItems.length; i++) {
      
      let title = String(rssResponseItems[i].title);
      // cleanDescription uses regex to scrape out the html tags from the responses and fix the spaces they leave
      let description = String(rssResponseItems[i].description)
      let descriptionWithoutTags = description.replace(/<\/?[^>]+(>|$)/g, " ");
      let cleanDescription = descriptionWithoutTags.replace(/  +/g, ' ');
      
      let link = String(rssResponseItems[i].link);
      let url = String(rssResponseItems[i].url);

      // var date = "0";
      // if (typeof String(rssResponseItems[i].created) != 'undefined') {
      let date = String(rssResponseItems[i].created);
      // }

      let imageUrl = String(rssResponseItems[i].media.content[1].url[0]);
  
      client.getSpace('695flcgy8uvq')
      .then((space) => space.createEntry('rssFeed', {
        fields: {
          title: {
            'en-GB': title
          },
          newsSource: {
            'en-GB': source
          },
          category: {
            'en-GB': rssCategory
          },
          description: {
            'en-GB': cleanDescription
          },
          link: {
            'en-GB': link
          },
          url: {
            'en-GB': url
          },
          date: {
            'en-GB': date
          },
          imageUrl: {
            'en-GB': imageUrl
          }
        }
      }))
      .then((entry) => entry.publish()) 
      .catch(console.error)
    }  
  },

  publishPolitocoEntriesToSpace: function(source, rssCategory, rssResponseItems) {
    for (i=0; i<rssResponseItems.length; i++) {
      
      let title = String(rssResponseItems[i].title);
      let description = String(rssResponseItems[i].description);
      let link = String(rssResponseItems[i].link);
      let url = String(rssResponseItems[i].url);
      if (typeof String(rssResponseItems[i].created) != 'undefined') {
        let date = String(rssResponseItems[i].created);
      } else {
        let date = "0";
      }
      // let imageURL = String(rssResponseItems[i].imageUrl)
  
      client.getSpace('695flcgy8uvq')
      .then((space) => space.createEntry('rssFeed', {
        fields: {
          title: {
            'en-GB': title
          },
          newsSource: {
            'en-GB': source
          },
          category: {
            'en-GB': rssCategory
          },
          description: {
            'en-GB': description
          },
          link: {
            'en-GB': link
          },
          url: {
            'en-GB': url
          },
          date: {
            'en-GB': date
          },
          imageUrl: {
            'en-GB': ""
          }
        }
      }))
      .then((entry) => entry.publish()) 
      .catch(console.error)
    }  
  }

}

exports.data = exportedMethods;