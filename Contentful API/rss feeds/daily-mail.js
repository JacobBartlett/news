// this dependency ('rss-to-json') will not work on WirelessDNET
var Feed = require('rss-to-json');
var contentful = require('../contentful-post.js');

var exportedMethods = {
  loadAllFeeds: function() {
    loadNewsFeed(2);
    loadScienceFeed(2);
    loadTravelFeed(2);
    loadMoneyFeed(2);
  }
}

function loadNewsFeed(number) {
  Feed.load('http://www.dailymail.co.uk/news/index.rss', function(err, rss) {
    processFeed(rss, number) 
  });
}

function loadScienceFeed(number) {
  Feed.load('http://www.dailymail.co.uk/sciencetech/index.rss', function(err, rss) {
    processFeed(rss, number) 
  });
}


function loadTravelFeed(number) {
  Feed.load('http://www.dailymail.co.uk/travel/index.rss', function(err, rss) {
    processFeed(rss, number) 
  });
}


function loadMoneyFeed(number) {
  Feed.load('http://www.dailymail.co.uk/money/index.rss', function(err, rss) {
    processFeed(rss, number) 
  });
}

function processFeed(rss, number) {
  let source = "The Daily Mail";
  let rssCategory = rss.title;
  let rssResponseItems = [];
  
  let maxLengthIncludingZero = rss.items.length - 1

  if (number == 0 || number > maxLengthIncludingZero) {
    for (i=0; i<rssResponseItems.length; i++) {
      rssResponseItems.push(rss.items[i])
    }
  } 
  else {
    for (i=0; i<number; i++) {
      rssResponseItems.push(rss.items[i])
    }
  }
  contentful.data.publishBBCEntriesToSpace(source, rssCategory, rssResponseItems)
}

exports.data = exportedMethods;

// // RSS Response Testing:
// Feed.load('http://www.dailymail.co.uk/articles.rss', function(err, rss) {

//   console.log("CATEGORY: " + rss.title);
//     for (i=0; i<4; i++) {
//       console.log(rss.items[i])
//       console.log("Title: " + rss.items[i].title);
//       console.log("Description: " + rss.items[i].description);
//       console.log("Link: " + rss.items[i].link);
//       console.log("URL: " + rss.items[i].url);
//       console.log("Created at: " + rss.items[i].created);
//       console.log("Image: " + rss.items[i].media.thumbnail[0].url[0]);
//     }
// });


