//
//  SyncInfo+CoreDataProperties.swift
//  
//
//  Created by Bartlett, Jacob (UK - London) on 06/02/2018.
//
//

import Foundation
import CoreData
import ContentfulPersistence


extension SyncInfo: SyncSpacePersistable {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<SyncInfo> {
        return NSFetchRequest<SyncInfo>(entityName: "SyncInfo")
    }

    static public let contentTypeId = "syncInfo"
    
    @NSManaged public var syncToken: String?

}
