//
//  RSSFeed+CoreDataProperties.swift
//
//
//  Created by Bartlett, Jacob (UK - London) on 20/02/2018.
//
//

import Foundation
import CoreData
import Contentful
import ContentfulPersistence

extension RSSFeedItem: EntryPersistable {
    
    @nonobjc public class func fetchRequest() -> NSFetchRequest<RSSFeedItem> {
        return NSFetchRequest<RSSFeedItem>(entityName: "RSSFeedItem")
    }
    
    static public let contentTypeId = "rssFeed"
    
    // Properties required by Contentful
    @NSManaged public var id: String
    @NSManaged public var localeCode: String
    @NSManaged public var createdAt: Date?
    @NSManaged public var updatedAt: Date?
    
    @NSManaged public var title: String?
    @NSManaged public var newsSource: String?
    @NSManaged public var category: String?
    @NSManaged public var bodyText: String?
    @NSManaged public var link: String?
    @NSManaged public var url: String?
    @NSManaged public var date: String?
    @NSManaged public var imageUrl: String?
    
    public static func fieldMapping() -> [FieldName: String] {
        return [
            "title" : "title",
            "newsSource": "newsSource",
            "category": "category",
            "description": "bodyText",
            "link": "link",
            "url": "url",
            "date": "date",
            "imageUrl": "imageUrl"
        ]
    }
    
}

