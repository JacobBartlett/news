//
//  FavouritedPublication+CoreDataProperties.swift
//  
//
//  Created by Bartlett, Jacob (UK - London) on 12/02/2018.
//
//

import Foundation
import CoreData


extension FavouritedPublication {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<FavouritedPublication> {
        return NSFetchRequest<FavouritedPublication>(entityName: "FavouritedPublication")
    }

    @NSManaged public var id: String
    @NSManaged public var dateFavourited: NSDate
    
    @NSManaged public var title: String?
    @NSManaged public var newsSource: String?
    @NSManaged public var category: String?
    @NSManaged public var bodyText: String?
    @NSManaged public var link: String?
    @NSManaged public var url: String?
    @NSManaged public var date: Date?
    @NSManaged public var imageUrl: String?
    
}
