//
//  Asset+CoreDataProperties.swift
//  
//
//  Created by Bartlett, Jacob (UK - London) on 06/02/2018.
//
//

import Foundation
import CoreData
import ContentfulPersistence


extension Asset: AssetPersistable {
    
    @nonobjc public class func fetchRequest() -> NSFetchRequest<Asset> {
        return NSFetchRequest<Asset>(entityName: "Asset")
    }
    
    static public let contentTypeId = "asset"
    
    // Properties required by Contentful
    @NSManaged public var id: String
    @NSManaged public var localeCode: String
    @NSManaged public var createdAt: Date?
    @NSManaged public var updatedAt: Date?
    
    @NSManaged public var assetDescription: String?
    @NSManaged public var title: String?
    @NSManaged public var urlString: String?
    @NSManaged public var publicationHeader: RSSFeedItem? 
    
}
