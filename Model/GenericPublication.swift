//
//  GenericPublication.swift
//  News
//
//  Created by Bartlett, Jacob (UK - London) on 22/02/2018.
//  Copyright © 2018 Deloitte. All rights reserved.
//

import Foundation

struct GenericPublication {
 
    var title: String?
    var newsSource: String?
    var category: String?
    var bodyText: String?
    var link: String?
    var url: String?
    var dateAsDate: Date?
    var dateAsString: String?
    var imageUrl: String?
    
    init(dateAsString: String?, dateAsDate: Date?, title: String?, category: String?, newsSource: String?, imageUrl: String?) {
        self.dateAsString = dateAsString
        self.dateAsDate = dateAsDate
        self.title = title
        self.category = category
        self.newsSource = newsSource
        self.imageUrl = imageUrl
    }
    
}

