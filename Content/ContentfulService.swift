//
//  ContentfulService.swift
//  News
//
//  Created by Bartlett, Jacob (UK - London) on 30/01/2018.
//  Copyright © 2018 Deloitte. All rights reserved.
//

import UIKit
import Contentful
import ContentfulPersistence
import Interstellar
import CoreData

class ContentfulService {
    
    static let shared = ContentfulService()
    static var syncSpace: SyncSpace?

    //keys for "content-management API key iOS":
    let spaceId = "695flcgy8uvq"
    let token = "a4d03b218a8a17f406b24b65f76cffc58677d9e0947a557102e8c412ca674bd5"

    let client: Client
    let syncManager: SynchronizationManager
    let store = CoreDataStore(context: DataManager.shared.mainContext())
    
    let model = PersistenceModel(spaceType: SyncInfo.self, assetType: Asset.self, entryTypes: [RSSFeedItem.self])

    init() {
        syncManager = SynchronizationManager(localizationScheme: .all, persistenceStore: store, persistenceModel: model)
        client = Client(spaceId: spaceId, accessToken: token, clientConfiguration: .default, sessionConfiguration: .default, persistenceIntegration: syncManager, contentTypeClasses: nil)
    }

    // MARK: - sync app with Contentful data
    static func sync(completion: @escaping (_ success: Bool, _ error: Error?) -> Void) {
        if let syncSpace = ContentfulService.syncSpace {
            ContentfulService.shared.client.nextSync(for: syncSpace, syncableTypes: .all) { (result) in
                switch result {
                case .success(let syncSpace):
                    
                    ContentfulService.syncSpace = syncSpace
                    completion(true, nil)
                case .error(let error):
                    completion(false, error)
                }
            }
        } else {
            ContentfulService.shared.client.initialSync { (result) in
                switch result {
                case .success(let syncSpace):
                    ContentfulService.syncSpace = syncSpace
                    completion(true, nil)
                case .error(let error):
                    completion(false, error)
                }
            }
        }
    }
    
}
