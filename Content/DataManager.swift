//
//  DataManager.swift
//  News
//
//  Created by Bartlett, Jacob (UK - London) on 05/02/2018.
//  Copyright © 2018 Deloitte. All rights reserved.
//

import Foundation
import UIKit
import CoreData

class DataManager {
    
    static let shared = DataManager()
    
    // MARK: - Core Data stack
    lazy var persistentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: "News")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
//    let mainContext: NSManagedObjectContext = shared.persistentContainer.viewContext
    
    @objc func mainContext() -> NSManagedObjectContext {
        return persistentContainer.viewContext
    }

}
