//
//  FavouriteService.swift
//  News
//
//  Created by Bartlett, Jacob (UK - London) on 12/02/2018.
//  Copyright © 2018 Deloitte. All rights reserved.
//

import Foundation
import CoreData

class FavouriteService {
    
    static let shared = FavouriteService()
    
    public func saveFavouritedPublication(_ publication: RSSFeedItem) {
        if publicationIsAlreadySaved(publication.id) == false {
            let context = DataManager.shared.mainContext()
            let entity = NSEntityDescription.entity(forEntityName: "FavouritedPublication", in: context)!
            let favPub = NSManagedObject(entity: entity, insertInto: context)
            
            favPub.setValue(publication.id, forKey: "id")
            favPub.setValue(Date() as NSDate, forKey: "dateFavourited")
            
            favPub.setValue(publication.title, forKey: "title")
            favPub.setValue(publication.newsSource, forKey: "newsSource")
            favPub.setValue(publication.category, forKey: "category")
            favPub.setValue(publication.bodyText, forKey: "bodyText")
            favPub.setValue(publication.link, forKey: "link")
            favPub.setValue(publication.url, forKey: "url")
            favPub.setValue(publication.date?.millisecondStringToDate(), forKey: "date")
            favPub.setValue(publication.imageUrl, forKey: "imageUrl")

            do {
                try context.save()
                print("Publication saved successfully!")
            } catch let error as NSError {
                print("Could not save \(error), \(error.userInfo)")
            }
        }
    }
    
    private func publicationIsAlreadySaved(_ id: String) -> Bool {
        if let favPubs = FavouriteService.shared.getAllFavourites() {
            for pub in favPubs {
                if pub.id == id {
                    return true
                }
            }
        }
        return false
    }
    
    public func removeFavouritedPublication(id: String) {
        let context = DataManager.shared.mainContext()
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "FavouritedPublication")
        do {
            let fetchedEntities = try context.fetch(fetchRequest)
            if let favourites = fetchedEntities as? [FavouritedPublication] {
                for favourite in favourites {
                    if favourite.id == id {
                        context.delete(favourite)
                    }
                }
            }
            do {
                try context.save()
            } catch let error as NSError {
                print("Could not save Dore Data context: \(error), \(error.userInfo)")
            }
        } catch {
            print("Error deleting item from Core Data: \(error)")
        }
    }
    
    public func getAllFavourites() -> [FavouritedPublication]? {
        let context = DataManager.shared.mainContext()
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "FavouritedPublication")
        do {
            let fetchedEntities = try context.fetch(fetchRequest)
            return fetchedEntities as? [FavouritedPublication]
        } catch {
            print("Could not fetch from Core Data: \(error)")
            return nil
        }
    }
    
}
