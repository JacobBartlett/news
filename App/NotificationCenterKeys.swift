//
//  NotificationCenterKeys.swift
//  News
//
//  Created by Bartlett, Jacob (UK - London) on 07/02/2018.
//  Copyright © 2018 Deloitte. All rights reserved.
//

let syncNotificationKey = "news.syncNotificationKey"
let favouriteRemovedNotificationKey = "news.favouriteRemovedNotificationKey"
