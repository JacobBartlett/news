//
//  AppDelegate.swift
//  News
//
//  Created by Bartlett, Jacob (UK - London) on 29/01/2018.
//  Copyright © 2018 Deloitte. All rights reserved.
//

import UIKit
import AirshipKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        UAirship.takeOff()
        UAirship.push().userPushNotificationsEnabled = true
        UAirship.push().defaultPresentationOptions = [.alert, .badge, .sound]
        
        ContentfulService.sync { (success, error) in
            if error == nil {
                print("Successfully synced with Contentful!")
                NotificationCenter.default.post(name: Notification.Name(syncNotificationKey), object: self) 
            } else {
                print("Contentful sync at launch error: \(error!)")
            }
        }
        return true
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        ContentfulService.sync { (success, error) in
            if error == nil {
                print("Successfully synced with Contentful!")
                NotificationCenter.default.post(name: Notification.Name(syncNotificationKey), object: self)
            } else {
                print("Contentful sync at launch error: \(error!)")
            }
        }
    }

}
