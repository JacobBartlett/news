var XMLHttpRequest = require("xmlhttprequest").XMLHttpRequest;
var DOMParser = require("dom-parser");

var xhr = new XMLHttpRequest();
executeXHR("Politico Politics");

function executeXHR(newsSource) {
    url = rssFeedURL(newsSource);
    console.log(url);
    xhr.open("GET", url, true);
    xhr.send();
    xhr.onreadystatechange = processRequest;
}

function processRequest() {
    console.log(xhr.readyState);
    console.log(xhr.status);
    if (xhr.readyState == 4 && xhr.status == 200) {
        handleXHRResponse(xhr)
    }
}

function handleXHRResponse() {
    //console.log(xhr.responseText);
    //console.log(xhr.responseXML);
    parser = new DOMParser();
    xmlDoc = parser.parseFromString(xhr.responseText, "application/xml");
    //console.log(xmlDoc);
    jsonDoc = xmlToJson(xmlDoc);
    console.log(jsonDoc);
}

function xmlToJson(xmlDoc) {
	var obj = {};

	if (xmlDoc.nodeType == 1) {
		if (xmlDoc.attributes.length > 0) {
		obj["@attributes"] = {};
			for (var j = 0; j < xmlDoc.attributes.length; j++) {
				var attribute = xmlDoc.attributes.item(j);
				obj["@attributes"][attribute.nodeName] = attribute.nodeValue;
			}
		}
	} else if (xmlDoc.nodeType == 3) { 
		obj = xmlDoc.nodeValue;
	}

	if (xmlDoc.hasChildNodes()) {
		for(var i = 0; i < xmlDoc.childNodes.length; i++) {
			var item = xmlDoc.childNodes.item(i);
			var nodeName = item.nodeName;
			if (typeof(obj[nodeName]) == "undefined") {
				obj[nodeName] = xmlToJson(item);
			} else {
				if (typeof(obj[nodeName].push) == "undefined") {
					var old = obj[nodeName];
					obj[nodeName] = [];
					obj[nodeName].push(old);
				}
				obj[nodeName].push(xmlToJson(item));
			}
		}
	}
	return obj;
}


// function XMLToJSON(xmlDoc) {
//     console.log("Doc Items");
//     responseItems = xmlDoc.getElementsByTagName("item");    
//     console.log(responseItems);
//     console.log(responseItems.length);

//     console.log(responseItems[0].childNodes[].nodeValue);

//     // for (i=0; i=responseItems.length; i++) {
//     //     console.log(responseItems[i].getElementsByTagName("title").innerText);
//     // }
// }

function rssFeedURL(sourceName) {
    switch (sourceName) {
        case "Politico Politics": 
            return "https://www.politico.com/rss/politics08.xml";
        default: 
            return "https://wiki.tei-c.org/images/d/d1/Test.xml";
    }
}