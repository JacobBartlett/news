var contentfulManagement = require('contentful-management');

// var allEntryIDsInSpace = [];

var client = contentfulManagement.createClient({
  // This is the access token for this space. Normally you get both ID and the token in the Contentful web app
  accessToken: 'CFPAT-89983f855135f9f2537ca092f48b75519cb17e0abbd96c093a32e58fd928d8a0'
})

var exportedMethods = {
    deleteAllEntriesInSpace: function() {
        unpublishAndDeleteAll();
    },
    removeDrafts: function() {
        deleteAllDrafts();
    }
}

function unpublishAndDeleteAll() {
    client.getSpace('695flcgy8uvq')
    .then((space) => space.getEntries())
    .then((response) => {
        for (i=0; i<response.items.length; i++) {
            let itemID = response.items[i].sys.id
            client.getSpace('695flcgy8uvq')
            .then((space) => space.getEntry(itemID))
            .then((entry) => entry.unpublish())
            .then((entry) => entry.delete())
            .then(() => console.log('Entry #' + itemID + ' deleted.'))
            .catch(console.error)
        }
    })
    .catch(console.error)
}

function deleteAllDrafts() {
    client.getSpace('695flcgy8uvq')
    .then((space) => space.getEntries())
    .then((response) => {
        for (i=0; i<response.items.length; i++) {
            let itemID = response.items[i].sys.id
            client.getSpace('695flcgy8uvq')
            .then((space) => space.getEntry(itemID))
            .then((entry) => entry.unpublish())
            .then((entry) => entry.delete())
            .then(() => console.log('Entry #' + itemID + ' deleted from drafts.'))
            .catch(console.error)
        }
    })
    .catch(console.error)
}

exports.data = exportedMethods;
