// this dependency ('rss-to-json') will not work on WirelessDNET
var Feed = require('rss-to-json');
var contentful = require('../contentful-post.js');

var exportedMethods = {
    loadAllFeeds: function() {
        loadWorldFeed(3);
        loadBusinessFeed(3);  
        loadTechnologyFeed(3);
        loadEducationFeed(3);
        loadScienceFeed(3);
        loadFootballFeed(3)
        loadDevelopmentFeed(3)
        loadCitiesFeed(3)
        loadObituariesFeed(3)
    }
}
  
function loadWorldFeed(number) {
    Feed.load('https://www.theguardian.com/world/rss', function(err, rss) {
        processFeed(rss, number) 
    });
}

function loadBusinessFeed(number) {
    Feed.load('https://www.theguardian.com/uk/business/rss', function(err, rss) {
        processFeed(rss, number)
    });
}

function loadTechnologyFeed(number) {
    Feed.load('https://www.theguardian.com/uk/technology/rss', function(err, rss) {
        processFeed(rss, number)
    });
}

function loadEducationFeed(number) {
    Feed.load('https://www.theguardian.com/education/rss', function(err, rss) {
        processFeed(rss, number)
    });
}

function loadScienceFeed(number) {
    Feed.load('https://www.theguardian.com/science/rss', function(err, rss) {
        processFeed(rss, number)
    });
}

function loadFootballFeed(number) {
    Feed.load('https://www.theguardian.com/football/rss', function(err, rss) {
        processFeed(rss, number)
    });
}

function loadDevelopmentFeed(number) {
    Feed.load('https://www.theguardian.com/global-development/rss', function(err, rss) {
        processFeed(rss, number)
    });
}

function loadCitiesFeed(number) {
    Feed.load('https://www.theguardian.com/cities/rss', function(err, rss) {
        processFeed(rss, number)
    });
}

function loadObituariesFeed(number) {
    Feed.load('https://www.theguardian.com/tone/obituaries/rss', function(err, rss) {
        processFeed(rss, number)
    });
}


function processFeed(rss, number) {
    let source = "The Guardian";
    let rssCategory = rss.title;
    let rssResponseItems = [];

    let maxLengthIncludingZero = rss.items.length - 1

    if (number == 0 || number > maxLengthIncludingZero) {
        for (i=0; i<rss.items.length.length; i++) {
            rssResponseItems.push(rss.items[i])
        }
    } 
    else {
        for (i=0; i<number; i++) {
            rssResponseItems.push(rss.items[i])
        }
    }

    contentful.data.publishGuardianEntriesToSpace(source, rssCategory, rssResponseItems)
}

exports.data = exportedMethods;
