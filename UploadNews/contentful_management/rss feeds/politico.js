// this dependency ('rss-to-json') will not work on WirelessDNET
var Feed = require('rss-to-json');
var contentful = require('../contentful-post.js');
// var imagesService = require('../automation/politico-images.js');

var exportedMethods = {
  loadAllFeeds: function() {
    loadCongressFeed(2);
    loadHealthCareFeed(2);
    loadDefenseFeed(2);
    loadEconomyFeed(2);
    loadEnergyAndEnvironmentFeed(2);
    loadPoliticsFeed(2);
  }
}

function loadCongressFeed(number) {
    Feed.load('https://www.politico.com/rss/congress.xml', function(err, rss) {
        processFeed(rss, number);
    });
} 

function loadHealthCareFeed(number) {
    Feed.load('https://www.politico.com/rss/healthcare.xml', function(err, rss) {
        processFeed(rss, number);
    });
} 

function loadDefenseFeed(number) {
    Feed.load('https://www.politico.com/rss/defense.xml', function(err, rss) {
        processFeed(rss, number);
    });
}

function loadEconomyFeed(number) {
    Feed.load('https://www.politico.com/rss/economy.xml', function(err, rss) {
        processFeed(rss, number);
    });
}

function loadEnergyAndEnvironmentFeed(number) {
    Feed.load('https://www.politico.com/rss/energy.xml', function(err, rss) {
        processFeed(rss, number);
    });
}

function loadPoliticsFeed(number) {
    Feed.load('https://www.politico.com/rss/politics08.xml', function(err, rss) {
        processFeed(rss, number);
    });
}

function processFeed(rss, number) {
    let source = "Politico";
    let rssCategory = rss.title;
    let rssResponseItems = [];

    let maxLengthIncludingZero = rss.items.length - 1

    if (number == 0 || number > maxLengthIncludingZero) {
        for (i=0; i<rss.items.length.length; i++) {
            rssRespnseItems.push(rss.items[i]);
            // item.imageUrl = await imagesService.data.automationService(item.url);
            // console.log("ITEM:")
            // console.log(item);
            // setTimeout( function() {
            //     rssRespnseItems.push(item);
            //     console.log(rssResponseItems); 
            // }, 10000); 
            // item.imageUrl = imagesService.data.automationService(item.url)
            
        }
    } 
    else {
        for (i=0; i<number; i++) {
            // item.imageUrl = await imagesService.data.automationService(item.url);
            rssResponseItems.push(rss.items[i]);

            // console.log("ITEM:")
            // console.log(item);
            // setTimeout( function() {
            //     rssResponseItems.push(item);
            //     console.log(rssResponseItems); 
            // }, 10000); 
        }
    }
    // setTimeout( function() {
        // console.log("ALL ITEMS:");
        // console.log(rssResponseItems); 
    contentful.data.publishPolitocoEntriesToSpace(source, rssCategory, rssResponseItems)
    // }, 10000);
}

exports.data = exportedMethods;

// loadEconomyFeed(1);

// imageUrl = imagesService.data.automationService("https://www.politico.com/story/2018/03/28/carter-page-fbi-justice-inspector-general-fisa-trump-490858");

// politico politics 
// Feed.load('https://www.politico.com/rss/politics08.xml', function(err, rss) {

//     console.log("CATEGORY: " + rss.title);
    
    // for (i=0; i<5; i++) { //i<rss.items.length; i++) {
    //     console.log("Title: " + rss.items[i].title);
    //     console.log("Description: " + rss.items[i].description);
    //     console.log("Link: " + rss.items[i].link);
    //     console.log("URL: " + rss.items[i].url);
    //     console.log("Created at: " + rss.items[i].created);
    //     // no image url available
    //     console.log("\n");
    // }
// });