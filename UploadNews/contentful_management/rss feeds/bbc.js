// this dependency ('rss-to-json') will not work on WirelessDNET
var Feed = require('rss-to-json');
var contentful = require('../contentful-post.js');

var exportedMethods = {
  loadAllFeeds: function() {
    loadBusinessFeed(4);
    loadTechnologyFeed(4);  
    loadEducationFeed(4);
    loadScienceAndEnvironmentFeed(4);
    loadPoliticsFeed(4);
    loadWorldFeed(4);
    loadUKFeed(4);
    loadEnglandFeed(4);
    loadScotlandFeed(4);
    loadWalesFeed(4);
    loadNIFeed(4);
    loadEntertainmentFeed(4);
  }
}

function loadBusinessFeed(number) {
  Feed.load('http://feeds.bbci.co.uk/news/business/rss.xml', function(err, rss) {
    processFeed(rss, number) 
  });
}

function loadTechnologyFeed(number) {
  Feed.load('http://feeds.bbci.co.uk/news/technology/rss.xml', function(err, rss) {
    processFeed(rss, number)
  });
}

function loadEducationFeed(number) {
  Feed.load('http://feeds.bbci.co.uk/news/education/rss.xml?edition=uk', function(err, rss) {
    processFeed(rss, number)
  });
}

function loadScienceAndEnvironmentFeed(number) {
  Feed.load('http://feeds.bbci.co.uk/news/science_and_environment/rss.xml?edition=uk', function(err, rss) {
    processFeed(rss, number)
  });
}

function loadPoliticsFeed(number) {
  Feed.load('http://feeds.bbci.co.uk/news/politics/rss.xml?edition=uk', function(err, rss) {
    processFeed(rss, number)
  });
}

function loadWorldFeed(number) {
  Feed.load('http://feeds.bbci.co.uk/news/world/rss.xml?edition=uk', function(err, rss) {
    processFeed(rss, number)
  });
}

function loadUKFeed(number) {
  Feed.load('http://feeds.bbci.co.uk/news/uk/rss.xml?edition=uk', function(err, rss) {
    processFeed(rss, number)
  });
}

function loadEnglandFeed(number) {
  Feed.load('http://feeds.bbci.co.uk/news/england/rss.xml?edition=uk', function(err, rss) {
    processFeed(rss, number)
  });
}

function loadScotlandFeed(number) {
  Feed.load('http://feeds.bbci.co.uk/news/scotland/rss.xml?edition=uk', function(err, rss) {
    processFeed(rss, number)
  });
}

function loadWalesFeed(number) {
  Feed.load('http://feeds.bbci.co.uk/news/wales/rss.xml?edition=uk', function(err, rss) {
    processFeed(rss, number)
  });
}

function loadNIFeed(number) {
  Feed.load('http://feeds.bbci.co.uk/news/northern_ireland/rss.xml?edition=uk', function(err, rss) {
    processFeed(rss, number)
  });
}

function loadEntertainmentFeed(number) {
  Feed.load('http://feeds.bbci.co.uk/news/entertainment_and_arts/rss.xml?edition=uk', function(err, rss) {
    processFeed(rss, number)
  });
}

function processFeed(rss, number) {
  let source = "BBC News";
  let rssCategory = rss.title;
  let rssResponseItems = [];
  
  let maxLengthIncludingZero = rss.items.length - 1

  if (number == 0 || number > maxLengthIncludingZero) {
    for (i=0; i<rss.items.length; i++) {
      rssResponseItems.push(rss.items[i])
    }
  } 
  else {
    for (i=0; i<number; i++) {
      rssResponseItems.push(rss.items[i])
    }
  }
  contentful.data.publishBBCEntriesToSpace(source, rssCategory, rssResponseItems)
}

exports.data = exportedMethods;

// // RSS Response Testing:
// Feed.load('http://feeds.bbci.co.uk/news/science_and_environment/rss.xml?edition=uk', function(err, rss) {
//     //console.log("CATEGORY: " + rss.title);
//     for (i=0; i<10; i++) { //i<rss.items.length; i++) {
//       // console.log("Title: " + rss.items[i].title);
//       // console.log("Description: " + rss.items[i].description);
//       // console.log("Link: " + rss.items[i].link);
//       // console.log("URL: " + rss.items[i].url);
//       // console.log("Created at: " + rss.items[i].created);
//       // console.log("Image: " + rss.items[i].media.thumbnail[0].url[0]);
//       // console.log("\n");
//       console.log(rss.items[i].media.thumbnail[0].url[0]);// );
//   }
// });

