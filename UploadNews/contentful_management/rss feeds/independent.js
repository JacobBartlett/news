// this dependency ('rss-to-json') will not work on WirelessDNET
var Feed = require('rss-to-json');
var contentful = require('../contentful-post.js');

var exportedMethods = {
  loadAllFeeds: function() {
    loadEducationFeed(3);
    loadMediaFeed(3);
    loadScienceFeed(3);
    loadEnvironmentFeed(3);
    loadSportFeed(3);
    loadLifestyleFeed(3);
    loadArtsFeed(3);
    loadTravelFeed(3);
  }
}

function loadEducationFeed(number) {
  Feed.load('http://www.independent.co.uk/news/education/rss', function(err, rss) {
    processFeed(rss, number) 
  });
}

function loadMediaFeed(number) {
  Feed.load('http://www.independent.co.uk/news/media/rss', function(err, rss) {
    processFeed(rss, number) 
  });
}


function loadScienceFeed(number) {
  Feed.load('http://www.independent.co.uk/news/science/rss', function(err, rss) {
    processFeed(rss, number) 
  });
}

function loadEnvironmentFeed(number) {
  Feed.load('http://www.independent.co.uk/environment/rss', function(err, rss) {
    processFeed(rss, number) 
  });
}

function loadSportFeed(number) {
  Feed.load('http://www.independent.co.uk/sport/rss', function(err, rss) {
    processFeed(rss, number) 
  });
}

function loadLifestyleFeed(number) {
  Feed.load('http://www.independent.co.uk/life-style/rss', function(err, rss) {
    processFeed(rss, number) 
  });
}

function loadArtsFeed(number) {
  Feed.load('http://www.independent.co.uk/arts-entertainment/rss', function(err, rss) {
    processFeed(rss, number) 
  });
}

function loadTravelFeed(number) {
  Feed.load('http://www.independent.co.uk/travel/rss', function(err, rss) {
    processFeed(rss, number) 
  });
}


function processFeed(rss, number) {
  let source = "The Independent";
  let rssCategory = rss.title;
  let rssResponseItems = [];

  let maxLengthIncludingZero = rss.items.length - 1

  if (number == 0 || number > maxLengthIncludingZero) {
    for (i=0; i<rss.items.length; i++) {
      console.log(rss.items[i])
      rssResponseItems.push(rss.items[i])
    }
  } 
  else {
    for (i=0; i<number; i++) {
      console.log(rss.items[i])
      rssResponseItems.push(rss.items[i])
    }
  }
  contentful.data.publishBBCEntriesToSpace(source, rssCategory, rssResponseItems) 
}

exports.data = exportedMethods;

// // RSS Response Testing:
// Feed.load('http://www.independent.co.uk/news/world/rss', function(err, rss) {

//   console.log("CATEGORY: " + rss.title);
//     for (i=0; i<4; i++) {
//       console.log(rss.items[i])
//       console.log("Title: " + rss.items[i].title);
//       console.log("Description: " + rss.items[i].description);
//       console.log("Link: " + rss.items[i].link);
//       console.log("URL: " + rss.items[i].url);
//       console.log("Created at: " + rss.items[i].created);
//       console.log("Image: " + rss.items[i].media.thumbnail[0].url[0]);
//     }
// });


