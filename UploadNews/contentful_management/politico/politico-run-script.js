var contentfulDelete = require('./contentful-delete.js');
var politico = require('./rss feeds/politico.js');

contentfulDelete.data.deleteAllEntriesInSpace();

setTimeout(loadAllFeeds, 60000); //delay to allow all entries to be deleted before refreshing items

function loadAllFeeds() {
    politico.data.loadAllFeeds();
}

