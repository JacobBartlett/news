const puppeteer = require('puppeteer');

var exportedMethods = {
  automationService: function(pageUrl) {
    getImage(pageUrl) 
  }
}

async function getImage(pageUrl) { 
  // const browser = await puppeteer.launch();
  const browser = await puppeteer.launch({headless: false});
  // headless determines whether the browser window actually appears
  
  const page = await browser.newPage();
  console.log("Accessing Politico...");
  // setTimeout(() => {
  //   page.goto(pageUrl);    
  // }, 5000);
  await page.goto(pageUrl);
  // await page.screenshot({path: '/Users/jacbartlett/Desktop/News/UploadNews/contentful-management/automation/images/example0.png'}); 

  const imageContainer = await page.evaluate(() => document.querySelector('picture').innerHTML);
  var regex = /<img[^>]+src="https([^">]+)/g;
  var src = "https" + regex.exec(imageContainer)[1];

  console.log("Image URL:");
  console.log(src);

  console.log("Closing automation service...");
  await browser.close();
  console.log("Complete!");
  return src;
}
  // })();
// }

// TESTING:
// getImage("https://www.politico.com/story/2018/03/29/scalise-shooting-house-speaker-490944");

exports.data = exportedMethods